#Guide to setting up nexus3 on OKD

oc new-project ta-nexus

oc create -f https://raw.githubusercontent.com/monodot/openshift-nexus/master/nexus3-persistent-template.yaml

oc new-app nexus3-persistent

#oc get pods -w

oc patch dc nexus -p '{"spec":{"template":{"spec":{"containers":[{"name":"nexus","ports":[{"containerPort": 5000,"protocol":"TCP","name":"docker"}]}]}}}}'

oc expose dc nexus --name=nexus-registry --port=5000

oc get route | grep registry

#Add nexus url as insecure registry in docker daemon.json
#Test connection using docker login <registry_url>
#If failure occures then configure Anonymous login on Nexus
#:)
